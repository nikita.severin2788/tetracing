﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager_NPC : MonoBehaviour {

	public float Speed;


	public GameObject Player;

	void Start(){
		Player = GameObject.Find ("Car");
	}

	void Update(){
		transform.Translate (Vector3.forward * Speed * Time.deltaTime);

		if (Player.transform.position.z > transform.position.z + 30f) {
			Destroy (gameObject);
		}
	}
		
	void OnTriggerEnter(Collider Col){
		if(Col.tag == "Bullet"){
			Destroy (gameObject);
		}
		if (Col.tag == "NPC") {
			Destroy (gameObject);
		}
	}
}

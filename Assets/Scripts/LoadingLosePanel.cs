﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingLosePanel : MonoBehaviour {

	public GameObject ProgressBar;
	public GameObject ProcentOut;


	void Start(){
		ProcentOut.GetComponent<Text> ().text = ProgressBar.GetComponent<ProgressScript> ().ProgressProcent.ToString () + "%";
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Advertisements;

public class Manager_Car : MonoBehaviour {


	public AudioSource Bullet_Sound;
	public AudioSource Take_Coin;

	public string LevelName;
	public bool Finih = false;

	public bool OneLuse = false;

	public int BulletCol = 0;
	public GameObject BulletSpawnPoint;
	public GameObject Bullet;

	public int Score;
	public int SpeedField = 1;
	public GameObject ScoreFieldText;
	public GameObject SpeedFieldText;
	public GameObject NeoCubeFiledText;
	public bool Lose;
	public bool Active;
	public bool Moving;
	public bool MovingLeft;
	public bool MovingRight;
	public bool StoryMode = false;
	public bool ScoreCountMode = false;

	public float Speed;
	public float RotationSpeed;
	public float SpeedAngel;

	public float SpeedFloating;


	public GameObject Body;

	float TimerSpeedAdd;
	float TimerScoreAdd;

	public GameObject SystemObject;

	void Start(){
		//SystemObject = GameObject.Find ("SystemObject");
		//Advertisement.Initialize ("1456235");


		SpeedFieldText.GetComponent<Text> ().text = SpeedField.ToString ();
		NeoCubeFiledText.GetComponent<Text> ().text = BulletCol.ToString ();

	}

	void Update(){
		if (Finih == true) {
			PlayerPrefs.SetString (LevelName, "Complite");
		}
		if (Active) {
			bool i = true;
			if (i){
				SpeedFieldText = GameObject.Find ("SpeedField");
				NeoCubeFiledText = GameObject.Find ("NeoCubeFeet");
				i = false;
			}
		}

		if (Active) {
			TimerSpeedAdd += Time.deltaTime;
			if (TimerSpeedAdd> 10) {
				SpeedField++;
				Speed++;
				SpeedFieldText.GetComponent<Text> ().text = SpeedField.ToString ();
				TimerSpeedAdd = 0;
			}
		}
		if (Active) {
			if (ScoreCountMode) {
				TimerScoreAdd += Time.deltaTime;
				if (TimerScoreAdd > 2) {
					Score++;
					if (StoryMode) {
						SystemObject.GetComponent<Manager_System> ().Score = this.Score;
						ScoreFieldText.GetComponent<Text> ().text = Score.ToString ();
					}
					TimerScoreAdd = 0;
				}
			}
		}
		if (Lose == true) {
			if (OneLuse == false) {
				SystemObject.GetComponent<Manager_Animations> ().StartCoroutine ("LoseMenu");
				StartCoroutine ("AdShow");	
				OneLuse = true;
			}
		}
	}


	void FixedUpdate () {
		int XPositionFloating = Convert.ToInt32( Math.Round ( transform.position.x, MidpointRounding.ToEven));
		if (Active) {
			if (Moving) {
				transform.Translate (Vector3.forward * Speed * Time.fixedDeltaTime);
			}

			if (MovingLeft == false && MovingRight == false) {
				transform.position = Vector3.Lerp (transform.position, new Vector3 (XPositionFloating, transform.position.y, transform.position.z), SpeedFloating * Time.deltaTime);
				Body.transform.rotation = Quaternion.Lerp (Body.transform.rotation, Quaternion.Euler (-90, -90, 0), SpeedAngel * Time.fixedDeltaTime);
			} else {
				Body.transform.rotation = Quaternion.Lerp (Body.transform.rotation, Quaternion.Euler (-90, -90, 0), SpeedAngel * Time.fixedDeltaTime);
			}

			if (MovingLeft && MovingRight) {
			} else {

				if (transform.position.x > -5.5f) {
					if (MovingLeft) {
						transform.Translate (Vector3.left * RotationSpeed * Time.fixedDeltaTime);
						Body.transform.rotation = Quaternion.Lerp (Body.transform.rotation, Quaternion.Euler (-75, -90, 0), SpeedAngel * Time.fixedDeltaTime);
					}
				}
				if (transform.position.x < 5.5f) {
					if (MovingRight) {
						transform.Translate (Vector3.right * RotationSpeed * Time.fixedDeltaTime);
						Body.transform.rotation = Quaternion.Lerp (Body.transform.rotation, Quaternion.Euler (-105, -90, 0), SpeedAngel * Time.fixedDeltaTime);
					}
				}
			}
		} else {
			Body.transform.rotation = Quaternion.Lerp (Body.transform.rotation, Quaternion.Euler (-90, -90, 0), SpeedAngel * Time.fixedDeltaTime);
		}
	}

	public void SpawnBullet(){
		if(Active){
		if (BulletCol > 0) {
			Bullet_Sound.Play ();
			Instantiate (Bullet, BulletSpawnPoint.transform.position, Quaternion.Euler (0, 0, 0));
			BulletCol--;
			NeoCubeFiledText.GetComponent<Text> ().text = BulletCol.ToString ();
			}
		}
	}

	IEnumerator AdShow(){
	int LoseCount = PlayerPrefs.GetInt ("LoseCount");
	Debug.Log(PlayerPrefs.GetInt ("LoseCount"));
	LoseCount = LoseCount + 1;
	PlayerPrefs.SetInt ("LoseCount", LoseCount);
		/*if (LoseCount % 5 == 0) {
			if (Advertisement.IsReady ()) {
				Debug.Log ("Add Show");
				Advertisement.Show ();
				StopCoroutine ("AdShow");
			}
		}
			StopCoroutine ("AdShow");*/
		yield return true;
	}

	void OnTriggerEnter(Collider Col){
		if (Col.tag == "NPC") {
			if (Finih == false) {
				Active = false;
				Lose = true;
			}
		}
		if (Col.tag == "Laser") {
			Active = false;
			Lose = true;
		}
		if (Col.tag == "BulletDrop") {
			BulletCol++;
			NeoCubeFiledText.GetComponent<Text> ().text = BulletCol.ToString ();
		}
		if (Col.tag == "CoinDrop") {
			int Coin = PlayerPrefs.GetInt("Coin");
			Coin++;
			PlayerPrefs.SetInt("Coin",Coin);
			Take_Coin.Play ();
		}
		if (Col.tag == "Finih") {
			Active = false;
			SystemObject.GetComponent<Manager_Animations> ().StartCoroutine ("FinihMenu");
			Finih = true;
		}
	}
}

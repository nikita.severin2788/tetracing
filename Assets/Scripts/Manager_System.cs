﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
//using GoogleMobileAds.Api;

public class Manager_System : MonoBehaviour {



	public GameObject BestScoreText;
	public GameObject ScoreText;

	public int Score;
	public int BestScore;


	public int NPCMin = 0;
	public int NPCMax = 20;

	public int	BulletMin= 20;
	public int	BulletMax= 30;

	public int	WallMin= 30;
	public int	WallMax= 40;

	public int DropCoinMin = 40;
	public int DropCoinMax = 60;

	void Start () {
		BestScore = PlayerPrefs.GetInt ("BestScore");
	}

	void Update () {
		
		if (Score > BestScore) {
			CheakScore (Score);
		}
	}

	public void Loadlevel(int NumScene){
		SceneManager.LoadScene (NumScene);
	}

	public void ShowMenu(){
		BestScore = PlayerPrefs.GetInt ("BestScore");
		BestScoreText.GetComponent<Text> ().text = BestScore.ToString ();
		ScoreText.GetComponent<Text> ().text = Score.ToString ();

	}

	public void CheakScore(int NewBestScore){
		PlayerPrefs.SetInt ("BestScore", NewBestScore);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserScript : MonoBehaviour {

	public float Speed;
	bool Derect;

	void Start(){
		Speed = Random.Range (1, 5);
		Derect = true;
	}
	void Update(){
		

		if (Derect) {
			transform.Translate (Vector3.left * Speed * Time.deltaTime);
		} else {
			transform.Translate (Vector3.right * Speed * Time.deltaTime);
		}

		if (transform.position.x > 7) {
			Derect = !Derect;
		}
		if (transform.position.x < -7) {
			Derect = !Derect;
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class SettingButtons : MonoBehaviour, IPointerUpHandler, IPointerDownHandler {

	public string Debug;
	public GameObject System;

	void Start(){
		System = GameObject.Find ("SystemObject");
		if (PlayerPrefs.GetString ("Shader") != "false" || PlayerPrefs.GetString ("Shader") != "true") {
			PlayerPrefs.SetString ("Shader", "false");
		}
	}

	public void OnPointerDown(PointerEventData eventData){

		switch (gameObject.name) {
		case"ShaderButton":

			if (PlayerPrefs.GetString ("Shader") == "false") {
				PlayerPrefs.SetString ("Shader", "true");
			} else {
				if (PlayerPrefs.GetString ("Shader") == "true") {
					PlayerPrefs.SetString ("Shader", "false");
				}
			}
			System.GetComponent<InitializeGame> ().ShaderUpdate ();
			break;
		}

	}
	public void OnPointerUp(PointerEventData eventData){


	}

	void Update(){
		Debug = PlayerPrefs.GetString ("Shader");
	}

}

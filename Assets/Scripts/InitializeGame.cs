﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;
public class InitializeGame : MonoBehaviour {

	public Material Car_Material;
	public Material Road_Material;

	public GameObject Camera;
	public GameObject Smoke;

	void Start(){

		Car_Material.color = new Color (PlayerPrefs.GetFloat ("Color_R_color1"), PlayerPrefs.GetFloat ("Color_G_color1"), PlayerPrefs.GetFloat ("Color_B_color1"), PlayerPrefs.GetFloat ("Color_A_color1"));
		Road_Material.color = new Color (PlayerPrefs.GetFloat ("Color_R_color2"), PlayerPrefs.GetFloat ("Color_G_color2"), PlayerPrefs.GetFloat ("Color_B_color2"), PlayerPrefs.GetFloat ("Color_A_color2"));

		Camera = GameObject.Find ("Main Camera");
		Smoke = GameObject.Find ("Smoke");

		if (PlayerPrefs.GetString ("Shader") == "false") {
			Camera.GetComponent<ColorCorrectionCurves> ().enabled = false;
			Smoke.SetActive (false);
		}
		if (PlayerPrefs.GetString ("Shader") == "true") {
			Camera.GetComponent<ColorCorrectionCurves> ().enabled = true;
			Smoke.SetActive (true);
		}
	}

	public void ShaderUpdate(){
		if (PlayerPrefs.GetString ("Shader") == "false") {
			Camera.GetComponent<ColorCorrectionCurves> ().enabled = false;
			Smoke.SetActive (false);
		}
		if (PlayerPrefs.GetString ("Shader") == "true") {
			Camera.GetComponent<ColorCorrectionCurves> ().enabled = true;
			Smoke.SetActive (true);
		}
	}
}

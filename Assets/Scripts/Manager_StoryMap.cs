﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
public class Manager_StoryMap : MonoBehaviour {


	public Animator Panel;

	[Header ("LevelNumOut")]
	public GameObject SetLevelObj;
	public GameObject AllLevelObj;

	public bool Delite;

	[Header("TextOut")]
	public GameObject LevelName;
	public GameObject Discription;
	public GameObject ProgressLine;


	[Header ("Controls")]
	public bool NexLevel = false;
	public bool BackLevel = false;
	public bool LoadScenes = false;

	public bool StartLoad;
	[Header ("NumberLevel")]
	public int LevelNumber;
	public int MaxLevel;

	void Start(){
		LevelNumber = 1;
		AllLevelObj.GetComponent<Text> ().text = MaxLevel.ToString ();
		SetLevelObj.GetComponent<Text> ().text = LevelNumber.ToString ();
		LoadInformation ();

	}

	void Update(){

		if (Delite == true) {
			PlayerPrefs.DeleteAll ();
		}

		if (NexLevel) {
			
			if (LevelNumber != MaxLevel) {
				
				LevelNumber++;
				SetLevelObj.GetComponent<Text> ().text = LevelNumber.ToString ();
				Panel.SetTrigger ("Right");
				LoadInformation ();
			}
			NexLevel = false;
		}

		if (BackLevel) {
			
			if (LevelNumber != 1) {
				
				LevelNumber--;
				SetLevelObj.GetComponent<Text> ().text = LevelNumber.ToString ();
				LoadInformation ();
				Panel.SetTrigger ("Left");
			}
			BackLevel = false;
		}
	}

	public void LoadInformation(){


			
		switch (LevelNumber.ToString()) {
		case"1":
			ProgressLine.GetComponent<Image> ().fillAmount = PlayerPrefs.GetFloat ("Level_1_Progress") / 100;
			Discription.GetComponent<Text> ().text = PlayerPrefs.GetFloat ("Level_1_Progress").ToString() + "%";
			LevelName.GetComponent<Text> ().text = "Grozny City";
			if (LoadScenes == true) {
				SceneManager.LoadScene ("Level_1");
				LoadScenes = false;
			}
			break;
		case"2":
			ProgressLine.GetComponent<Image> ().fillAmount = PlayerPrefs.GetFloat ("Level_2_Progress") / 100;
			Discription.GetComponent<Text> ().text = PlayerPrefs.GetFloat ("Level_2_Progress").ToString() + "%";
			LevelName.GetComponent<Text> ().text = "Zoom Valley";
			if (LoadScenes == true) {
				SceneManager.LoadScene ("Level_2");
				LoadScenes = false;
			}
			break;

		case"3":
			ProgressLine.GetComponent<Image> ().fillAmount = PlayerPrefs.GetFloat ("Level_3_Progress") / 100;
			Discription.GetComponent<Text> ().text = PlayerPrefs.GetFloat ("Level_3_Progress").ToString() + "%";
			LevelName.GetComponent<Text> ().text = "Achkhoy City";
			if (LoadScenes == true) {
				SceneManager.LoadScene ("Level_3");
				LoadScenes = false;
			}
			break;

		case"4":
			ProgressLine.GetComponent<Image> ().fillAmount = PlayerPrefs.GetFloat ("Level_4_Progress") / 100;
			Discription.GetComponent<Text> ().text = PlayerPrefs.GetFloat ("Level_4_Progress").ToString() + "%";
			LevelName.GetComponent<Text> ().text = "City 17";
			if (LoadScenes == true) {
				SceneManager.LoadScene ("Level_1");
				LoadScenes = false;
			}
			break;

		case"5":
			ProgressLine.GetComponent<Image> ().fillAmount = PlayerPrefs.GetFloat ("Level_5_Progress") / 100;
			Discription.GetComponent<Text> ().text = PlayerPrefs.GetFloat ("Level_5_Progress").ToString() + "%";
			LevelName.GetComponent<Text> ().text = "Ghost Town";
			if (LoadScenes == true) {
				SceneManager.LoadScene ("Level_5");
				LoadScenes = false;
			}
			break;

		case"6":
			ProgressLine.GetComponent<Image> ().fillAmount = PlayerPrefs.GetFloat ("Level_6_Progress") / 100;
			Discription.GetComponent<Text> ().text = PlayerPrefs.GetFloat ("Level_6_Progress").ToString() + "%";
			LevelName.GetComponent<Text> ().text = "Strange Valley";
			if (LoadScenes == true) {
				SceneManager.LoadScene ("Level_6");
				LoadScenes = false;
			}
			break;

		case"7":
			ProgressLine.GetComponent<Image> ().fillAmount = PlayerPrefs.GetFloat ("Level_7_Progress") / 100;
			Discription.GetComponent<Text> ().text = PlayerPrefs.GetFloat ("Level_7_Progress").ToString() + "%";
			LevelName.GetComponent<Text> ().text = "Saturn City";
			if (LoadScenes == true) {
				SceneManager.LoadScene ("Level_7");
				LoadScenes = false;
			}
			break;

		case"8":
			ProgressLine.GetComponent<Image> ().fillAmount = PlayerPrefs.GetFloat ("Level_8_Progress") / 100;
			Discription.GetComponent<Text> ().text = PlayerPrefs.GetFloat ("Level_8_Progress").ToString() + "%";
			LevelName.GetComponent<Text> ().text = "Souls City";
			if (LoadScenes == true) {
				SceneManager.LoadScene ("Level_8");
				LoadScenes = false;
			}
			break;

		case"9":
			ProgressLine.GetComponent<Image> ().fillAmount = PlayerPrefs.GetFloat ("Level_9_Progress") / 100;
			Discription.GetComponent<Text> ().text = PlayerPrefs.GetFloat ("Level_9_Progress").ToString() + "%";
			LevelName.GetComponent<Text> ().text = "Voted City";
			if (LoadScenes == true) {
				SceneManager.LoadScene ("Level_9");
				LoadScenes = false;
			}
			break;

		case"10":
			ProgressLine.GetComponent<Image> ().fillAmount = PlayerPrefs.GetFloat ("Level_10_Progress") / 100;
			Discription.GetComponent<Text> ().text = PlayerPrefs.GetFloat ("Level_10_Progress").ToString() + "%";
			LevelName.GetComponent<Text> ().text = "End City";
			if (LoadScenes == true) {
				SceneManager.LoadScene ("Level_10");
				LoadScenes = false;
			}
			break;
		}
	}
}

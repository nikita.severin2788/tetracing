﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager_Camera : MonoBehaviour {

	[Header("Setting")]
	public GameObject Player;
	public float CameraSpeed;
	public float z_position;
	public float y_position;
	public float x_position;
	public bool GameMode = false;

	void Start () {

		Player = GameObject.Find ("Car");

	}

	void FixedUpdate () {
		if (Player != null) {
			if (GameMode == false) {
				transform.position = Vector3.Lerp (transform.position, new Vector3 (Player.transform.position.x + x_position, Player.transform.position.y + y_position, Player.transform.position.z + z_position), CameraSpeed * Time.fixedDeltaTime);
			} else {
				transform.position = Vector3.Lerp (transform.position, new Vector3 (transform.position.x + x_position, Player.transform.position.y + y_position, Player.transform.position.z + z_position), CameraSpeed * Time.fixedDeltaTime);
			}
		} else {
			Debug.Log (gameObject.name + ": Player not find!");
		}
	}
}

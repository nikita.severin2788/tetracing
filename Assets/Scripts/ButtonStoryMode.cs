﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;


public class ButtonStoryMode : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

	public Animator BlackScreen;
	public GameObject StoryMap;

	void Start(){
		StoryMap = GameObject.Find ("StoryMap");
	}

	public void OnPointerDown(PointerEventData eventData){
		switch (gameObject.name) {
		case"LoadButton":
			StoryMap.GetComponent<Manager_StoryMap> ().LoadScenes = true;
			StoryMap.GetComponent<Manager_StoryMap> ().LoadInformation ();
			Debug.Log ("Yes");
			break;
		case"NextButton":
			StoryMap.GetComponent<Manager_StoryMap> ().NexLevel = true;
			Debug.Log ("Yes");
			break;
		case"BackButton":
			StoryMap.GetComponent<Manager_StoryMap> ().BackLevel = true;
			Debug.Log ("Yes");
			break;
		case"BackToStart":
			StartCoroutine ("GoToGameMapMenu");
			break;
		
		}
	}

	public IEnumerator GoToGameMapMenu(){
		BlackScreen.SetTrigger ("Out");
		yield return new WaitForSeconds (1);
		SceneManager.LoadScene ("MainScene");

	}

	public void OnPointerUp(PointerEventData eventData){

	}

}

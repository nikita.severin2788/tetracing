﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Manager_PropsGenerator : MonoBehaviour {

	[Header("Settings")]
	public bool StoryMode = false;

	public int NPCMin = 0;
	public int NPCMax = 20;

	public int	BulletMin= 20;
	public int	BulletMax= 30;

	public int	WallMin= 30;
	public int	WallMax= 40;

	public int	CoinDropMin= 40;
	public int	CoinDropMax= 50;

	[Header("Prefabs")]
	public GameObject CarNPC;
	public GameObject Bullet;
	public GameObject Wall;
	public GameObject Coin;

	GameObject SystemObject;

	void Start(){ //Initialize
		if (StoryMode == true) {
			SystemObject = GameObject.Find ("SystemObject");
			NPCMax = SystemObject.GetComponent<Manager_System> ().NPCMax;
			NPCMin = SystemObject.GetComponent<Manager_System> ().NPCMin;
			BulletMax = SystemObject.GetComponent<Manager_System> ().BulletMax;
			BulletMin = SystemObject.GetComponent<Manager_System> ().BulletMin;
			WallMax = SystemObject.GetComponent<Manager_System> ().WallMax;
			WallMin = SystemObject.GetComponent<Manager_System> ().WallMin;
			CoinDropMax = SystemObject.GetComponent<Manager_System> ().DropCoinMax;
			CoinDropMin = SystemObject.GetComponent<Manager_System> ().DropCoinMin;
		}
		Generator ();
	}

	void Generator(){//StartPropGenerate
		int RandomValue = UnityEngine.Random.Range (0, 100);
		int PositionValueZ = UnityEngine.Random.Range(Convert.ToInt32(transform.position.z - 3),Convert.ToInt32(transform.position.z + 3));
		if (RandomValue >= NPCMin && RandomValue < NPCMax) {//NPS Generator
			int PositionValueX = UnityEngine.Random.Range(-5,5);
			Instantiate (CarNPC, new Vector3 (PositionValueX,transform.position.y + 0.3f, transform.position.z), Quaternion.Euler(0,0,0));
		}

		if (RandomValue >= BulletMin && RandomValue < BulletMax) {//Bullet Generator
			int PositionValueX = UnityEngine.Random.Range(-7,7);
			Instantiate (Bullet, new Vector3 (PositionValueX,  transform.position.y + 0.7f, PositionValueZ), Quaternion.Euler(0,0,0));
		}

		if (RandomValue >= WallMin && RandomValue < WallMax) {//Wall Generator
			int PositionZ = UnityEngine.Random.Range(0,10);
			Instantiate (Wall, new Vector3 (transform.position.x, transform.position.y,PositionValueZ), Quaternion.Euler(0,0,0));
		}
		if (RandomValue >= CoinDropMin && RandomValue < CoinDropMax) {//Coin Generator
			int PositionValueX = UnityEngine.Random.Range(-7,7);
			Instantiate (Coin, new Vector3 (PositionValueX,  transform.position.y + 0.7f, PositionValueZ), Quaternion.Euler(0,0,0));
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDrop : MonoBehaviour {

	public float Speed;

	void Update(){
		transform.Rotate (Vector3.up, Speed * Time.deltaTime);
	}

	void OnTriggerEnter(Collider Col){
		if (Col.tag == "Car") {
			Destroy (gameObject);
		}
	}
}

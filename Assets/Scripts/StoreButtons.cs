﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class StoreButtons : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

	public Color Default;
	public Color Select;

	void Update(){
		switch (gameObject.name) {
		case"SelectCarColor":
			if ("1" == PlayerPrefs.GetString ("SelectMode")) {
				gameObject.GetComponent<Image> ().color = Select;
			} else {
				gameObject.GetComponent<Image> ().color = Default;
			}
			break;

		case"SelectRoadColor":
			if ("2" == PlayerPrefs.GetString ("SelectMode")) {
				gameObject.GetComponent<Image> ().color = Select;
			} else {
				gameObject.GetComponent<Image> ().color = Default;
			}
			break;
		}
	}

	public void OnPointerDown(PointerEventData eventData){

		switch (gameObject.name) {
		case"SelectCarColor":
			PlayerPrefs.SetString ("SelectMode", "1");
			gameObject.GetComponent<Image> ().color = Select;
			break;

		case"SelectRoadColor":
			PlayerPrefs.SetString ("SelectMode", "2");
			break;
		}
	}

	public void OnPointerUp(PointerEventData eventData){

	}

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class ButtonStartScreen : MonoBehaviour, IPointerUpHandler, IPointerDownHandler {

	public AudioSource Click;

	public GameObject SystemObject;
	public Manager_Animations AnimSystem;

	public Animator BlackScreen;

	void Start(){
		SystemObject = GameObject.Find ("SystemObject");
		AnimSystem = SystemObject.GetComponent<Manager_Animations>();
	}

	public void OnPointerDown(PointerEventData eventData){

		GameObject Camera = GameObject.Find ("Main Camera");

		switch (gameObject.name) {
		case "GoStartButton":
			//Click.Play ();
			AnimSystem.StartCoroutine ("StoreScreenShowOut");
			Camera.GetComponent<Manager_Camera> ().y_position = 10;
			Camera.GetComponent<Manager_Camera> ().z_position = -13.5f;
			Camera.GetComponent<Manager_Camera> ().x_position = -0;
			Camera.GetComponent<Manager_Camera> ().CameraSpeed = 2;

			break;
		case "StoreButton":
			//Click.Play ();
			AnimSystem.StartCoroutine ("StoreScreenShowIn");
			Camera.GetComponent<Manager_Camera> ().y_position = 6;
			Camera.GetComponent<Manager_Camera> ().z_position = -6;
			Camera.GetComponent<Manager_Camera> ().x_position = -3;
			Camera.GetComponent<Manager_Camera> ().CameraSpeed = 2;
			Camera.GetComponent<Manager_Camera> ().GameMode = false;
			break;
		case"RestartButton":
			//Click.Play ();
			AnimSystem.StartCoroutine ("Restart");
			break;
		case "StartButton":
			//Click.Play ();
			AnimSystem.StartCoroutine ("StartScreenShowOut");

			Camera.GetComponent<Manager_Camera> ().y_position = 6;
			Camera.GetComponent<Manager_Camera> ().z_position = -6;
			Camera.GetComponent<Manager_Camera> ().CameraSpeed = 2;
			Camera.GetComponent<Manager_Camera> ().GameMode = true;
			break;
		
		case "StoryModeButton":
			//Click.Play ();
			StartCoroutine ("GoToGameMapMenu");
			break;
		case "BackToMainMenu":
			//Click.Play ();
			StartCoroutine ("GoToGameMapMenu");
			break;
		}
	}

	public IEnumerator GoToGameMapMenu(){
		BlackScreen.SetTrigger ("Out");
		yield return new WaitForSeconds (1);
		SceneManager.LoadScene ("GameMapMenu");

	}

	public void OnPointerUp(PointerEventData eventData){

	}


}

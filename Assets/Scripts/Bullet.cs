﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	public float Speed;
	GameObject Player;

	void Start(){
		Player = GameObject.Find ("Car");
		Speed += Player.GetComponent<Manager_Car> ().Speed;
	}

	void Update(){
		transform.Translate (Vector3.forward * Speed * Time.deltaTime);
		if (transform.position.z > Player.transform.position.z + 40f) {
			Destroy (gameObject);
		}
	}

	void OnTriggerEnter(Collider Col){
		if (Col.tag == "NPC") {
			Destroy (gameObject);
		}
	}
}

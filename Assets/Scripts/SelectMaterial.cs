﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SelectMaterial : MonoBehaviour, IPointerDownHandler, IPointerUpHandler{

	public int Price = 25;

	public int Money;

	public Material CarMaterial_1;
	public Material CarMaterial_2;
	public Material RoadMaterial_1;
	public Material RoadMaterial_2;

	public GameObject Lock;

	public string SelectMode; // 1-car1, 2-car2, 3-road1, 4-road2.

	void Start(){
		PlayerPrefs.SetString ("SelectMode", "1");

		if (PlayerPrefs.GetString (gameObject.name) == "true") {
			Lock.SetActive (false);
		} else {
			Lock.SetActive (true);
		}
	}

	public void OnPointerDown(PointerEventData eventData){



		if (PlayerPrefs.GetString (gameObject.name) == "true") {
			Lock.SetActive (false);
			SelectMode = PlayerPrefs.GetString ("SelectMode");

			switch (SelectMode) {
			case"1":
				CarMaterial_1.color = GetComponent<Image> ().color;
				PlayerPrefs.SetFloat("Color_R_color1", GetComponent<Image> ().color.r);
				PlayerPrefs.SetFloat("Color_G_color1", GetComponent<Image> ().color.g);
				PlayerPrefs.SetFloat("Color_B_color1", GetComponent<Image> ().color.b);
				PlayerPrefs.SetFloat("Color_A_color1", GetComponent<Image> ().color.a);
				break;
			case"2":
				CarMaterial_2.color = GetComponent<Image> ().color;
				PlayerPrefs.SetFloat("Color_R_color2", GetComponent<Image> ().color.r);
				PlayerPrefs.SetFloat("Color_G_color2", GetComponent<Image> ().color.g);
				PlayerPrefs.SetFloat("Color_B_color2", GetComponent<Image> ().color.b);
				PlayerPrefs.SetFloat("Color_A_color2", GetComponent<Image> ().color.a);
				break;
			
			case"3":
				RoadMaterial_1.color = GetComponent<Image> ().color;
				break;

			case"4":
				RoadMaterial_2.color = GetComponent<Image> ().color;
				break;
			}



		} else {
		Lock.SetActive (true);
			if (PlayerPrefs.GetInt ("Coin") >= 25) {
				int AllCoins = PlayerPrefs.GetInt ("Coin");
				AllCoins = AllCoins - 25;
				PlayerPrefs.SetInt ("Coin", AllCoins);
				PlayerPrefs.SetString (gameObject.name, "true");
				LockUpdate ();
			}
		}
	}
	public void OnPointerUp(PointerEventData eventData){

	}

	void LockUpdate(){
		Lock.SetActive (false);
	}
}
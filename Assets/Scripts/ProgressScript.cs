﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ProgressScript : MonoBehaviour {
	[Header ("ConnectObjects")]
	public GameObject ProggressBar;
	public GameObject Player;
	public GameObject SystemObject;
	[Header ("Counts")]
	public int GenerationAll;
	public float SetDistance;
	[Range(0,100)]
	public float ProgressProcent;
	public float Distance;

	public string LevelName;
	public float BestProcent;
	void Start () {

		Player = GameObject.Find ("Car");

		LevelName = LevelName + "_Progress";
		GenerationAll = SystemObject.GetComponent<Manager_LevelGenerator> ().MaxGenerator;
		Distance = ((GenerationAll + 7) * 6) - 2.50f;
		BestProcent = PlayerPrefs.GetFloat (LevelName);
	}


	void Update () {
		SetDistance = Player.transform.position.z;
		float Procent = SetDistance / Distance; //ProgressLine вычисления

		if (ProgressProcent < 100) {
			ProgressProcent =  Mathf.RoundToInt(Procent * 100); // процентные вычмсления
		} else {
			PlayerPrefs.SetFloat (LevelName, 100f);
			ProgressProcent = 100f;
		}
		if (BestProcent < ProgressProcent) {
			PlayerPrefs.SetFloat (LevelName, ProgressProcent);
			BestProcent = ProgressProcent;
		}
		ProggressBar.GetComponent<Image> ().fillAmount = Procent;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class BannerStart : MonoBehaviour {


	void Start () {
		StartCoroutine ("BannerOut");
	}

	IEnumerator BannerOut(){
		yield return new WaitForSeconds (2f);
		GetComponent<Animator> ().SetTrigger ("Out");
		yield return new WaitForSeconds (1f);
		SceneManager.LoadScene ("MainScene");
	}
}

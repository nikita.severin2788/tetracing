﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class FinihButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

	public string NextLevel;
	public string ThisLevel;

	public void OnPointerDown(PointerEventData eventData){


		switch (gameObject.name) {
		case"RestartButton":
			SceneManager.LoadScene (ThisLevel);
			break;
		case "NextLevelButton":
			SceneManager.LoadScene (NextLevel);
			break;
		case "Menu":
			SceneManager.LoadScene ("GameMapMenu");
			break;
		}
	}
	public void OnPointerUp(PointerEventData evnetData){

	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager_LevelGenerator : MonoBehaviour {

	public GameObject FinihSegment;

	public int CountSegmetn;
	public int MinGenerateSegment;

	public Vector3 StepAddUnit;

	public GameObject Segmet;

	public int MaxGenerator = 1000;
	public int PlatformsCount;
	public bool LevelGeneratorEnd;

	void Start () {
	
		StartCoroutine ("Generator");

	}
	IEnumerator Generator(){
		while (true) {
			if (LevelGeneratorEnd) {
				if(PlatformsCount < MaxGenerator){
					while (CountSegmetn < MinGenerateSegment) {
						PlatformsCount++;
						Instantiate (Segmet, StepAddUnit, Quaternion.Euler (-90, 0, 0));
						StepAddUnit = new Vector3 (StepAddUnit.x, StepAddUnit.y, StepAddUnit.z + 6);
						CountSegmetn++;
					}
					}else{
						Instantiate (FinihSegment, StepAddUnit, Quaternion.Euler (-90, 0, 0));
						StepAddUnit = new Vector3 (StepAddUnit.x, StepAddUnit.y, StepAddUnit.z + 6);
						for(int i = 0; i < 15; i++){
							Instantiate (Segmet, StepAddUnit, Quaternion.Euler (-90, 0, 0));
							StepAddUnit = new Vector3 (StepAddUnit.x, StepAddUnit.y, StepAddUnit.z + 6);
						}
					StopCoroutine ("Generator");
					}
					
			} else {
				while (CountSegmetn < MinGenerateSegment) {
					Instantiate (Segmet, StepAddUnit, Quaternion.Euler (-90, 0, 0));
					StepAddUnit = new Vector3 (StepAddUnit.x, StepAddUnit.y, StepAddUnit.z + 6);
					CountSegmetn++;
					yield return true;
				}
			}
			yield return true;
		}
	}
}


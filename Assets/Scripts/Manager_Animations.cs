﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
//using GoogleMobileAds.Api;
//using UnityEngine.Advertisements;

public class Manager_Animations : MonoBehaviour {


	public bool StoryMode = false;

	//public string adUnitId = "ca-app-pub-9897552434336939/9736165407";

	[Header("Level")]
	public string LevelName;

	[Header("Finih")]
	public GameObject FinihMenuObj;
	public GameObject StoryModeLoseObj;

	[Header ("Camera")]
	public GameObject Camera;
	public Vector3 MoveTorwardDefault;
	public Vector3 MoveTorwardToStore;
	public float SpeedTowardToStore;

	[Header ("Numbers")]
	public GameObject Num1;
	public GameObject Num2;
	public GameObject Num3;
	public GameObject Go;

	[Header ("Screens")]
	public GameObject GameScreen;
	public GameObject StartScreen;
	public GameObject StoreScreen;


	[Header ("StartScreen")]
	public Animator Bar;
	public Animator StartButton;
	public Animator Logo;


	[Header ("GameMode")]
	public GameObject Player;
	public GameObject ControlButtons;
	public Animator ControlButtonsAnimator;
	public Animator FeetBar;
	public GameObject LoseMenuObject;

	[Header("System")]
	public GameObject SystemObject;
	public GameObject BalckScreen;


	void Start(){
		Player = GameObject.Find ("Car");
		Camera = GameObject.Find ("Main Camera");
		
	}

	void Update(){
	}

	public IEnumerator Restart(){
		//yield return new WaitForSeconds (1);

		SceneManager.LoadScene (LevelName);
		yield return true;
	}

	public IEnumerator LoseMenu(){

		LoseMenuObject.SetActive (true);
		if (StoryMode == true) {
			gameObject.GetComponent<Manager_System> ().ShowMenu ();
		}
		ControlButtonsAnimator.SetTrigger ("Out");
		StopCoroutine ("LoseMenu");
		yield return true;

	}

	public IEnumerator GoGame(){
		GameScreen.SetActive (true);
		Num3.SetActive (true);
		yield return new WaitForSeconds (1);
		Num3.SetActive (false);
		Num2.SetActive (true);
		yield return new WaitForSeconds (1);
		Num2.SetActive (false);
		Num1.SetActive (true);
		yield return new WaitForSeconds (1);
		Num1.SetActive (false);
		ControlButtons.SetActive (true);
		Go.SetActive (true);
		yield return new WaitForSeconds (1);
		Camera.GetComponent<Manager_Camera> ().CameraSpeed = 10;
		Player.GetComponent<Manager_Car> ().Active = true;
		Go.SetActive (false);
		StartCoroutine ("AdView");

		StopCoroutine ("GoGame");
		/*if (BannerStyle.IsLoaded ()) {
			BannerStyle.Show();
		}*/
		yield return true;
	}

	public void StartScreenShowIn(){
		
	
	}
	public IEnumerator StartScreenShowOut(){
		if (StoryMode) {
			Bar.SetTrigger ("Out");
		}
			StartButton.SetTrigger ("Out");
			Logo.SetTrigger ("Out");
			yield return new WaitForSeconds (1);
			StartScreen.SetActive (false);
			StartCoroutine ("GoGame");
			yield return true;
	}

	public IEnumerator StoreScreenShowIn(){
		StartButton.GetComponent<ButtonStartScreen> ().enabled = false;
		if (StoryMode) {
			Bar.SetTrigger ("Out");
		}
		Logo.SetTrigger ("Out");
		StartButton.SetTrigger ("Out");
		yield return new WaitForSeconds (1);
		GameScreen.SetActive (false);
		StoreScreen.SetActive (true);
		yield return true;
		//
	}

	public IEnumerator StoreScreenShowOut(){
		StoreScreen.SetActive (false);
		StartScreen.SetActive (true);
		Bar.SetTrigger ("In");
		Logo.SetTrigger ("In");
		StartButton.SetTrigger ("In");
		yield return new WaitForSeconds (2);
		StartButton.GetComponent<ButtonStartScreen> ().enabled = true;
		yield return true;
	}

	public IEnumerator FinihMenu(){

		FinihMenuObj.SetActive (true);
		PlayerPrefs.SetString ("Level_1", "Complite");

		yield return true;
	}

	public IEnumerator AdView(){
		/*BannerView BannerV = new BannerView(adUnitId, AdSize.Banner, AdPosition.Top);
		AdRequest request = new AdRequest.Builder ()
			.AddTestDevice(AdRequest.TestDeviceSimulator)       // Simulator.
			.AddTestDevice("3977A1A2871EE4BE")  // My test device.
			.Build();
		BannerV.LoadAd (request);
		*/
		yield return true;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ControlPanelScript : MonoBehaviour, IPointerUpHandler, IPointerDownHandler, IPointerExitHandler {

	public GameObject Player;
	Manager_Car ControlCar;

	void Start(){
		Player = GameObject.Find ("Car");
		ControlCar = Player.GetComponent<Manager_Car> ();
	}


	public void OnPointerDown(PointerEventData eventData){

		switch (gameObject.name) {
		case"ButtonLeft":
			ControlCar.MovingLeft = true;
			break;

		case"ButtonRight":
			ControlCar.MovingRight = true;
			break;
		}

	}

	public void OnPointerUp(PointerEventData eventData){
		switch (gameObject.name) {
		case"ButtonLeft":
			ControlCar.MovingLeft = false;
			break;

		case"ButtonRight":
			ControlCar.MovingRight = false;
			break;
		}
	}

	public void OnPointerExit(PointerEventData eventData){
		switch (gameObject.name) {
		case"ButtonLeft":
			ControlCar.MovingLeft = false;
			break;

		case"ButtonRight":
			ControlCar.MovingRight = false;
			break;
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager_Platform : MonoBehaviour {


	public GameObject Player;
	public GameObject SystemGame;
	Manager_LevelGenerator SystemPlatform;

	void Start () {


		SystemGame = GameObject.Find ("SystemObject");
		Player = GameObject.Find ("Car");
		SystemPlatform = SystemGame.GetComponent<Manager_LevelGenerator> ();
	}

	void Update () {
		if (Player.transform.position.z > transform.position.z + 30f) {
			SystemPlatform.CountSegmetn--;
			Destroy (gameObject);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonGameScreen : MonoBehaviour, IPointerUpHandler, IPointerDownHandler, IPointerExitHandler, IPointerEnterHandler {

	public GameObject Car;

	public GameObject UIGame;
	public GameObject UIStopMode;

	void Start(){
		if (gameObject.name == "PlayImg") {
			gameObject.GetComponent<RawImage> ().color = new Color (255, 255, 255, 255);
			Time.timeScale = 0;
		}
		if (gameObject.name == "ControlButtonLeft" || gameObject.name == "ControlButtonRight" || gameObject.name == "BulletButtonPanel") {
			Car = GameObject.Find ("Car");
		}

	}

	public void OnPointerDown(PointerEventData eventData){

		switch (gameObject.name) {
		case "ControlButtonLeft":
			Car.GetComponent<Manager_Car> ().MovingLeft = true;
			break;
		case "ControlButtonRight":
			Car.GetComponent<Manager_Car> ().MovingRight = true;
			break;
		case"BulletButtonPanel":
			Car.GetComponent<Manager_Car>().SpawnBullet();
			break;
		case"StopButton":
			Time.timeScale = 0;
			UIStopMode.SetActive (true);
			UIGame.SetActive (false);
			break;
		case"PlayImg":
			Time.timeScale = 0.2f;
			gameObject.GetComponent<RawImage> ().color = new Color (255, 255, 255, 30);
			break;
		}

	}



	public void OnPointerUp(PointerEventData eventData){

		switch (gameObject.name) {
		case "ControlButtonLeft":
			Car.GetComponent<Manager_Car> ().MovingLeft = false;
			break;
		case "ControlButtonRight":
			Car.GetComponent<Manager_Car> ().MovingRight = false;
			break;
		case"PlayImg":
			Time.timeScale = 1f;
			UIStopMode.SetActive (false);
			UIGame.SetActive (true);
			break;
		}
	}

	public void OnPointerEnter(PointerEventData eventData){
		switch (gameObject.name) {
		case"PlayImg":
			gameObject.GetComponent<RawImage> ().color = new Color (255, 255, 255, 255);
		break;
	}
	}

	public void OnPointerExit(PointerEventData eventData){
		switch (gameObject.name) {
		case "ControlButtonLeft":
			Car.GetComponent<Manager_Car> ().MovingLeft = true;
			break;
		case "ControlButtonRight":
			Car.GetComponent<Manager_Car> ().MovingRight = true;
			break;
		}
	}
}
